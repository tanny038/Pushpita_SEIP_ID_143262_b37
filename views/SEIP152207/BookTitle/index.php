
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;
if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html lang="en">

<head>

 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>

 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <title>Book Title - Index</title>


</head>

<body>

<?php
$objBookTitle=new BookTitle();



$recordSet = $objBookTitle->index();
echo "<table border='5px'><tr>Fetched all as ASSOC</tr>";
foreach ($recordSet as $record ){
 echo "<tr>";
 echo "<td>".$record['id']."</td>";
 echo "<td>".$record['book_title']."</td>";
 echo "<td>".$record['author_name']."</td>";
 echo "</tr>";

}
echo "</table>";


echo "<br> ";

$recordSet = $objBookTitle->index("OBJ");
echo "<table border='5px'><tr>Fetched all as OBJ</tr>";
foreach ($recordSet as $record ){
 echo "<tr>";
 echo "<td>".$record->id."</td>";
 echo "<td>".$record->book_title."</td>";
 echo "<td>".$record->author_name."</td>";

 echo "<td> <a href=\"view.php?id=$record->id\"  class=\"btn btn-info\" role=\"button\">View</a> </td> ";
 echo "<td> <a href=\"edit.php?id=$record->id\"  class=\"btn btn-info\" role=\"button\">Edit</a> </td> ";
 echo "<td> <a href=\"delete.php?id=$record->id\"  class=\"btn btn-info\" role=\"button\">Delete</a> </td> ";
 echo "<td> <a href=\"trash.php?id=$record->id\"  class=\"btn btn-info\" role=\"button\">trash</a> </td> ";
 echo "</tr>";

}
echo "</table>";



?>


</body>

</html>



<script>
 $('#message').show().delay(10).fadeOut();
 $('#message').show().delay(10).fadeIn();
 $('#message').show().delay(10).fadeOut();
 $('#message').show().delay(10).fadeIn();
 $('#message').show().delay(1200).fadeOut();
</script>
