<?php

namespace App\Email;

use App\Model\Database as DB;

use App\Message\Message;
use App\Utility\Utility;

class Email extends DB
{

    public $id='';

    public $user_name='';

    public $email_address='';


    public function __construct()
    {

        parent::__construct();

    }

    public function index()
    {
        echo "Email index";

    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('email_address',$data))
        {

            $this->email_address=$data['email_address'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->email_address);


        $sql="insert into email(user_name,email_address)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [EmailAddress: $this->email_address ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [EmailAddress: $this->email_address ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }

}// end of BookTitle class