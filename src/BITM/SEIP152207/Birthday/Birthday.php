<?php

namespace App\Birthday;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;


class Birthday extends DB
{

    public $id='';

    public $username='';

    public $birthday='';


    public function __construct()
    {

        parent::__construct();

    }



    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('username',$data))
        {

            $this->username=$data['username'];
        }
        if (array_key_exists('birthday',$data))
        {

            $this->birthday=$data['birthday'];
        }

    }
    public function store()
    {
        $arrData=array($this->username,$this->birthday);


        $sql="insert into birthday(username,birthday)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ Username: $this->username ] , [ BirthDay: $this->birthday ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[UserName: $this->username] , [BirthDay: $this->birthday ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }

    public function index($fetchMode = "ASSOC")
    {
        $STH = $this->DBH->query('SELECT * from book_title');


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;



    }

    public function view($fetchMode = "ASSOC")
    {
        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetch();
        return $arrAllData;



    }








}