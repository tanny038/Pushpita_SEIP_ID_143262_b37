<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{

    public $id;

    public $book_title;

    public $author_name;


    public function __construct()
    {

        parent::__construct();

    }//end of construct

    public function SetData($postVariabledata=NULL)
    {
        if(array_key_exists("id",$postVariabledata))
        {
           $this->id =$postVariabledata['id'];
        }
        if(array_key_exists("book_title",$postVariabledata))
        {
            $this->book_title =$postVariabledata['book_title'];
        }
        if(array_key_exists("author_name",$postVariabledata))
        {
            $this->author_name =$postVariabledata['author_name'];
        }

    }//end of set



 public  function store()

 {
     $arrData=array($this->book_title,$this->author_name);
     $sql="insert into book_title(book_title,author_name) VALUES (?,?)";
    $STH= $this->DBH->prepare($sql);
     $STH->execute();
     $result=$STH->execute($arrData);
     if($result)
     {
         Message::Message("Data Has been Inserted sucesfully :)");
     }
     else
         Message::Message("Failed! Data Has Not Been Inserted Succesfully");

     Utility::redirect("create.php");

 }//endofstore

    public function index($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from book_title');


        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }

    public function view($fetchMode="ASSOC"){

        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode, "OBJ")>0)   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }

    public function update(){

        $arrData  = array($this->book_title,$this->author_name);

        $sql = 'UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }
    public function delete(){

        $sql = "DELETE FROM book_title WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }

}// end of BookTitle class